function yprime = sirode(t,y,p);
%SIRODE: ODE for SIR epidemic model.
yprime= [-p(1)*y(1)*y(2);p(1)*y(1)*y(2)-p(2)*y(2)];